/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2013, Thomas Stibor <thomas@stibor.net>
 */

#include <string.h> 
#include <unistd.h>
#include "gpio.h"
#include "lcd.h"

static int gpio_fds[6];

const unsigned char const LCD_PINS[6] = {
    LCD_RS,
    LCD_E,
    LCD_D4,
    LCD_D5,
    LCD_D6,
    LCD_D7
};

static void lcd_byte(const char bits, const unsigned char mode)
{
    gpio_set_value(LCD_RS, mode);

    /* High bits. */
    gpio_set_value(LCD_D4, 0);
    gpio_set_value(LCD_D5, 0);
    gpio_set_value(LCD_D6, 0);
    gpio_set_value(LCD_D7, 0);
    if ((bits & 0x10) == 0x10)
	gpio_set_value(LCD_D4, 1);

    if ((bits & 0x20) == 0x20)
	gpio_set_value(LCD_D5, 1);

    if ((bits & 0x40) == 0x40)
	gpio_set_value(LCD_D6, 1);

    if ((bits & 0x80) == 0x80)
	gpio_set_value(LCD_D7, 1);

    /* Toggle 'Enable' pin.*/
    sleep(E_DELAY);
    gpio_set_value(LCD_E, 1);
    sleep(E_PULSE);
    gpio_set_value(LCD_E, 0);
    sleep(E_DELAY);

    /* Low bits. */
    gpio_set_value(LCD_D4, 0);
    gpio_set_value(LCD_D5, 0);
    gpio_set_value(LCD_D6, 0);
    gpio_set_value(LCD_D7, 0);
    if ((bits & 0x01) == 0x01)
	gpio_set_value(LCD_D4, 1);

    if ((bits & 0x02) == 0x02)
	gpio_set_value(LCD_D5, 1);

    if ((bits & 0x04) == 0x04)
	gpio_set_value(LCD_D6, 1);

    if ((bits & 0x08) == 0x08)
	gpio_set_value(LCD_D7, 1);

    /* Toggle 'Enable' pin.*/
    sleep(E_DELAY);
    gpio_set_value(LCD_E, 1);
    sleep(E_PULSE);
    gpio_set_value(LCD_E, 0);
    sleep(E_DELAY);
}

void lcd_clear()
{
    lcd_byte(0x01, LCD_CMD);	/* Clear display. */
}

void lcd_init()
{
    lcd_byte(0x33, LCD_CMD);	/* Initialization. */
    lcd_byte(0x32, LCD_CMD);	/* Initialization. */
    lcd_byte(0x28, LCD_CMD);	/* 4-Bit interface data, two line display, 5x8 dot character font. */
    lcd_byte(0x0C, LCD_CMD);	/* Display on, no cursor, no blinking. */
    lcd_byte(0x06, LCD_CMD);	/* Move curser to the right after character input. */
    lcd_clear();
}

void lcd_open_gpio()
{
    unsigned char i;

    for (i = 0; i < 6; i++) {
	gpio_export(LCD_PINS[i]);
	gpio_set_dir(LCD_PINS[i], 1); /* 1 denotes output direction. */
	gpio_fds[i] = gpio_fd_open(LCD_PINS[i]);
    }
}

void lcd_close_gpio()
{
    unsigned char i;

    for (i = 0; i < 6; i++) {
	gpio_fd_close(gpio_fds[i]);
	gpio_unexport(LCD_PINS[i]);
    }

}

void lcd_string(const char *text, 
		const unsigned char lcd_line)
{
    unsigned char i;

    if (lcd_line != LCD_LINE_1 && lcd_line != LCD_LINE_2)
	return;

    lcd_byte(lcd_line, LCD_CMD);
    for (i = 0; i < LCD_WIDTH; i++) {
	if (i < strlen(text))
	    lcd_byte(text[i], LCD_CHR);
	else
	    lcd_byte(' ', LCD_CHR);
    }
}
