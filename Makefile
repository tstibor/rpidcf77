CC     = gcc
LIBS   = -lrt
CFLAGS = -Wall -O2

HEADERS       = gpio.h lcd.h
OBJECTS       = gpio.o lcd.o 
OBJECTS_DCF77 = rpidcf77.o
OBJECTS_LCD   = lcd_test.o
TARGET_DCF77  = rpidcf77
TARGET_LCD    = lcd_test

INIT_SCRIPT   = rpidcf77_initscript.sh

default: $(TARGET_DCF77) $(TARGET_LCD)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

rpidcf77: $(OBJECTS) $(OBJECTS_DCF77)
	$(CC) $(OBJECTS) $(OBJECTS_DCF77) $(LIBS) -o $@

lcd_test: $(OBJECTS) $(OBJECTS_LCD)
	$(CC) $(OBJECTS) $(OBJECTS_LCD) $(LIBS) -o $@

install: $(TARGET_DCF77)
	cp -f $(TARGET_DCF77) /usr/sbin
	cp -f $(INIT_SCRIPT) /etc/init.d/$(TARGET_DCF77)

clean:
	rm -f $(OBJECTS) $(OBJECTS_DCF77) $(OBJECTS_LCD)
	rm -f $(TARGET_DCF77) $(TARGET_LCD)
