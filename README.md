# Overview #

Raspberry Pi daemon for receiving and decoding DCF77 signal. 

See screencasts:

* [http://www.stibor.net/screencasts/rpidcf77_software_screencast.ogv (Software)](http://www.stibor.net/screencasts/rpidcf77_software_screencast.ogv)

* [http://www.stibor.net/screencasts/rpidcf77_lcd_hardware_software_screencast.ogg (Hardware)](http://www.stibor.net/screencasts/rpidcf77_lcd_hardware_software_screencast.ogg)