/* Define GPIO to LCD mapping */
#define LCD_RS 7
#define LCD_E  8
#define LCD_D4 25
#define LCD_D5 24
#define LCD_D6 23
#define LCD_D7 18

#define LCD_WIDTH 16 /* Maximum characters per line. */
#define LCD_CHR 1
#define LCD_CMD 0

#define LCD_LINE_1  0x80 /* LCD RAM address for the 1st line. */
#define LCD_LINE_2  0xC0 /* LCD RAM address for the 2nd line. */

#if 0  /* 20x4 LCD, will be used in later projects
	Note: #define LCD_WIDTH 20 */
#define LCD_LINE_3  0x94 /* LCD RAM address for the 3rd line. */
#define LCD_LINE_4  0xD4 /* LCD RAM address for the 4th line. */
#endif

/* Timing constants. */
#define E_PULSE 0.00005
#define E_DELAY 0.00005

void lcd_open_gpio();
void lcd_close_gpio();
void lcd_init();
void lcd_clear();
void lcd_string(const char *text, 
		const unsigned char lcd_line);
