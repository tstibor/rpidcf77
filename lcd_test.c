#include <stdio.h>
#include <unistd.h>
#include "lcd.h"

int main(int argc, char *argv[])
{
    unsigned char i;
    char str[LCD_WIDTH] = {0};

    lcd_open_gpio();
    lcd_init();
    lcd_string("Testing", LCD_LINE_1);
    sleep(1);
    for (i = 0; i < LCD_WIDTH; i++) {
	str[i] = '*';
	lcd_string(str, LCD_LINE_2);
	sleep(1);
    }

    lcd_string("It Works :-)", LCD_LINE_1);
    lcd_string("It Works :-)", LCD_LINE_2);

    lcd_close_gpio();

    return 0;
}
