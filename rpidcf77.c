/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2013, Thomas Stibor <thomas@stibor.net>
 */

/*
 * For kernel "Linux raspberrypi 3.18.7-v7+", the poll() call
 * returns immediately, and thus the timing measure routine
 * does not work correctly. See also https://lkml.org/lkml/2015/1/29/641
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <poll.h>
#include <time.h>
#include <unistd.h>
#include <argp.h>
#include <syslog.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "gpio.h"
#include "lcd.h"

#define POLL_TIMEOUT (5 * 1000) /* 5 seconds */

#define MIN           0
#define HRS           1
#define DAY           2
#define WDAY          3
#define MONTH         4
#define YEAR          5
#define DECODE_ERROR  255

#define PID_LOCK_FILE    "/var/run/rpidcf77.pid"

typedef enum {syncing = 0, decoding = 1, operating = 2} dcf77_status;

typedef struct {
    unsigned char sec;		/* 0..59, note that we are 1 sec. behind the true DCF77 sec,
				 as we encode the sec. with the falling flank. For this reason
				 + 1 sec is added in the print time functions. Moreover, when we set
				 the system time in the sync flank we correct the seconds by +2,
				 as the sync flank is ca. 1800/1900 ms. */
    unsigned char min;		/* 0..59 */
    unsigned char hour;		/* 0..23 */
    unsigned char day;		/* 1..31 */
    unsigned char wday;		/* 1..7 */
    unsigned char month;	/* 1..12 */
    unsigned char year;		/* 00..99 */
    unsigned char cest;		/* 0,1 (false, true)*/
    dcf77_status status;	/* dcf77_status: 0,1,2 */
} dcf77_type;

const char const *WEEKDAYS[7] = {
    "Sun",
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat"
};

const char const *MODES[3] = {
    "(waiting for sync minute)",
    "(decoding seconds)",
    "(operating)"
};

/* Strings to be displayed in LCD. */
char lcd_str1[LCD_WIDTH];	
char lcd_str2[LCD_WIDTH];

const char *argp_program_version = "rpidcf77 version 0.9";
const char *argp_program_bug_address = "Thomas Stibor <thomas@stibor.net>";
     
/* Program documentation. */
static char doc[] = "Daemon that reads dcf77 time signal via gpio, prints time on LCD and updates system time";

/* The options we understand. */
struct argp_option options[] = {
    { "gpio",      'g', "NUM",  0, "GPIO pin wired to dcf77 signal [required]"},
    { "daemon",    'd',  0,     0, "Run as daemon in background"},
    { "synctime",  's',  0,     0, "Synchronize and set system time with received dcf77 date and time data"},
    { "tolerance", 't', "NUM",  0, "Tolerance of low/high length, default 20ms"},
    {0}
};

     
/* Used by main to communicate with parse_opt. */
struct arguments {
    int gpio, tolerance, daemon, synctime;
};

static struct arguments arguments;
static int pid_fh;
static unsigned int gpio;
static int gpio_fd;
static dcf77_type dcf77 = {0}; 

/* Parse a single option. */
static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
    struct arguments *arguments = state->input;

    switch (key)
    {
    case 'g': {
	if (arg) 
	    arguments->gpio = atoi(arg);
	break;
    }
    case 'd': {
	arguments->daemon = 1;
	break;
    }
    case 's': {
	arguments->synctime = 1;
	break;
    }
    case 't': {
	if (arg)
	    arguments->tolerance = atoi(arg);
	break;
    }
    case ARGP_KEY_END: {
	if (arguments->gpio < 1) {
	    printf("Missing or incorrect -g, --gpio=NUM given\n");
	    argp_usage(state);
	} else if (arguments->tolerance < 0) {
	    printf("Incorrect -t, --tolerance[=NUM] given\n");
	    argp_usage(state);
	}
    }
    default:
	return ARGP_ERR_UNKNOWN;
    }
    
    return 0;
}

static struct argp argp = { options, parse_opt, 0, doc };

unsigned char parity(const unsigned char const bit_array[], 
		     const unsigned char from, const unsigned char to)
{
    unsigned char i;
    unsigned char result = 0;
 
    for (i = from; i <= to; i++)
	result ^=bit_array[i];
    
    return result;
}

unsigned char bin_to_dec(const unsigned char const bit_array[], const unsigned char len)
{
    unsigned char i;
    unsigned char result = 0;

    for (i = 0; i < len; i++)
	result += bit_array[i] * (1 << i);

    return result;
}

unsigned char concat(const unsigned char x, const unsigned char y)
{
    char str1[8], str2[8];

    snprintf(str1, 3, "%d", x);
    snprintf(str2, 3, "%d", y);
    strcat(str1, str2);

    return atoi(str1);
}

void append_bits(const unsigned char const bit_array[], char out_str[], const unsigned char j)
{
    unsigned char i;
    unsigned char k = 0;

    memset(out_str, 0, LCD_WIDTH);

    if (j < LCD_WIDTH)
	for (i = 0; i < j; i++) 
	    out_str[i] = bit_array[i] == 0 ? '0' : '1';
    else
	for (i = j - LCD_WIDTH; i < j; i++, k++) 
	    out_str[k] = bit_array[i] == 0 ? '0' : '1';
}

unsigned char decode(const unsigned const char bit_array[], const unsigned char what)
{
    unsigned char result;

    switch (what) {
    case MIN:
	result = concat(bin_to_dec(&bit_array[25], 3), bin_to_dec(&bit_array[21], 4));
	break;
    case HRS:
	result = concat(bin_to_dec(&bit_array[33], 2), bin_to_dec(&bit_array[29], 4));
	break;
    case DAY:
	result = concat(bin_to_dec(&bit_array[40], 2), bin_to_dec(&bit_array[36], 4));
	break;
    case WDAY:
	result = bin_to_dec(&bit_array[42], 3);
	break;
    case MONTH:
	result = concat(bin_to_dec(&bit_array[49], 1), bin_to_dec(&bit_array[45], 4));
	break;
    case YEAR:
	result = concat(bin_to_dec(&bit_array[54], 4), bin_to_dec(&bit_array[50], 4));
	break;
    default:
	result = DECODE_ERROR;
    }

    return result;
}

dcf77_status decode_dcf77(const unsigned char const bit_array[], dcf77_type *dcf77)
{
    /* Check whether data is correctly received. */
    unsigned char i;
    for (i = 0; i < 59; i++)
    	if (bit_array[i] != 0 && bit_array[i] != 1)
    	    goto out;
    
    /* Bit 0 is always 0 (start of minute), restart syncing otherwise */
    if (bit_array[0] != 0)
    	goto out;

    /* Start bit is always 1 (start of encoded time), restart syncing otherwise */
    if (bit_array[20] != 1)
    	goto out;

    /* Even parity mismatch, restart syncing otherwise */
    if (parity(bit_array, 21, 28) != 0 ||
    	parity(bit_array, 29, 35) != 0 ||
    	parity(bit_array, 36, 58) != 0)
    	goto out;

    dcf77->cest = bit_array[17];
    dcf77->min = decode(bit_array, MIN);
    dcf77->hour = decode(bit_array, HRS);
    dcf77->day = decode(bit_array, DAY);
    dcf77->wday = decode(bit_array, WDAY);
    dcf77->month = decode(bit_array, MONTH);
    dcf77->year = decode(bit_array, YEAR);

    return operating;

out:
    if (!arguments.daemon)
	printf("Received dcf77 data cannot be decoded due to bit errors\n");
    lcd_string("dcf77 signal", LCD_LINE_1);
    lcd_string("has errors", LCD_LINE_2);
    return syncing;
}

void print_dcf77(const unsigned char const bit_array[], const dcf77_type *dcf77)
{
    printf("*** Encoded DCF77 ***\n");
    printf("Reserve antenna : %2d\n", bit_array[15]);
    printf("CEST            : %2d\n", dcf77->cest);
    printf("Start bit       : %2d\n", bit_array[20]);
    printf("Minutes         : %2d\n", dcf77->min);
    printf("Hours           : %2d\n", dcf77->hour);
    printf("Day             : %2d\n", dcf77->day);
    printf("Day of week     : %2d\n", dcf77->wday);
    printf("Month           : %2d\n", dcf77->month);
    printf("Year            : %2d\n", dcf77->year);
    printf("Parity bit min  : %2d\n", bit_array[28]);
    printf("Parity bit hour : %2d\n", bit_array[35]);
    printf("Parity bit date : %2d\n", bit_array[58]);
    printf("Parity min      : %2d\n", parity(bit_array, 21, 28));
    printf("Parity hour     : %2d\n", parity(bit_array, 29, 35));
    printf("Parity date     : %2d\n", parity(bit_array, 36, 58));
    printf("*********************\n");
}

void print_lcd(const unsigned char const bit_array[], 
	       const dcf77_type *dcf77, 
	       const float time_diff_ms) 
{
    if (dcf77->status == syncing) {
	lcd_string("waiting for sync", LCD_LINE_1);
	sprintf(lcd_str2, "flank: %4.2f ms", time_diff_ms);
	lcd_string(lcd_str2, LCD_LINE_2);
    } else if (dcf77->status == decoding) {
	if (dcf77->sec < 59) {
	    sprintf(lcd_str1, "decode bit: %2d", dcf77->sec);
	    lcd_string(lcd_str1, LCD_LINE_1);
	    append_bits(bit_array, lcd_str2, dcf77->sec);
	    lcd_string(lcd_str2, LCD_LINE_2);
	}
    } else {
	sprintf(lcd_str1, " %02d:%02d:%02d  %4s", dcf77->hour, dcf77->min, (dcf77->sec + 1) % 60, /* second adjustment, correct 1 behind. */
		dcf77->cest == 0 ? "CET" : "CEST");
	sprintf(lcd_str2, " %s %02d.%02d.20%02d", WEEKDAYS[dcf77->wday % 7], /* day in week adjustment (1,2,...,7) -> (0,1,..6).  */
		dcf77->day, dcf77->month, dcf77->year); 
	lcd_string(lcd_str1, LCD_LINE_1);
	lcd_string(lcd_str2, LCD_LINE_2);
    }
}

void print_coding(const unsigned char const bit_array[], 
		   const dcf77_type *dcf77, 
		   const float time_diff_ms)
{
    if (dcf77->status == syncing)
	printf("Bit %2d: flank length: %4.2f ms %s\n", dcf77->sec, time_diff_ms, 
	       MODES[dcf77->status]);
    else if (dcf77->status == decoding)    
	printf("Coding bit %2d: %d (%4.2f ms) %s\n", dcf77->sec, bit_array[dcf77->sec], 
	       time_diff_ms, MODES[dcf77->status]);
    else
	printf("Dcf77:  %02d:%02d:%02d  %4s, %s %02d.%02d.20%02d, coding bit %2d: %d (%4.2f ms) %s\n", 
	       dcf77->hour, dcf77->min, (dcf77->sec + 1) % 60, dcf77->cest == 0 ? "CET" : "CEST",
	       WEEKDAYS[dcf77->wday % 7], dcf77->day, dcf77->month, dcf77->year,
	       dcf77->sec, bit_array[dcf77->sec],
	       time_diff_ms, MODES[dcf77->status]);
}

void set_system_time(const dcf77_type const *dcf77)
{
    struct tm *time_info;
    time_t raw_time;
    char sync_str[128];
    memset(sync_str, 0, 128);

    if (dcf77->status != operating) /* Do not synchronize system time when the dcf77 signal is not fully decoded. */
	return;

    /* Read current time and modify it with precise dcf77 time. */
    time(&raw_time);
    time_info = localtime(&raw_time);

    /*
      tm_sec    The number of seconds after the minute, normally in the range 0 to 59, but can be up to 60 to allow for leap seconds.
      tm_min    The number of minutes after the hour, in the range 0 to 59.
      tm_hour   The number of hours past midnight, in the range 0 to 23.
      tm_mday   The day of the month, in the range 1 to 31.
      tm_mon    The number of months since January, in the range 0 to 11.
      tm_year   The number of years since 1900.
      tm_wday   The number of days since Sunday, in the range 0 to 6.
      tm_yday   The number of days since January 1, in the range 0 to 365.
      tm_isdst  A flag that indicates whether daylight saving time is in effect at the time described.
                The value is positive if daylight saving time is in effect, zero if it is not, and 
		negative if the information is not available.
     */
    time_info->tm_sec  = dcf77->sec;
    time_info->tm_min  = dcf77->min;         
    time_info->tm_hour = dcf77->hour;       
    time_info->tm_mday = dcf77->day;        
    time_info->tm_mon  = dcf77->month - 1;        
    time_info->tm_wday = dcf77->wday - 1;      
    time_info->tm_yday = dcf77->year - 1900; /* The number of years since 1900. */
    time_info->tm_isdst = dcf77->cest; /* A flag that indicates whether daylight saving time is in effect at the time described.  
					      The value is positive if daylight saving time is in effect, zero if it is not, and negative if the
					      information is not available. */
    
    raw_time = mktime(time_info);
    struct timeval tv = {raw_time, 0};
    sprintf(sync_str, "%02d:%02d:%02d  %4s, %s %02d.%02d.20%02d, syncing system time with dcf77 result: %s",
	    dcf77->hour, dcf77->min, dcf77->sec, dcf77->cest == 0 ? "CET" : "CEST",
	    WEEKDAYS[dcf77->wday % 7], dcf77->day, dcf77->month, dcf77->year,
	    settimeofday(&tv, NULL) == 0 ? "successful" : "failed");
    if (arguments.daemon)
	syslog(LOG_INFO, sync_str);
    else
	printf("%s\n", sync_str); 
}

void cleanup()
{
    lcd_clear();
    gpio_fd_close(gpio_fd);
    gpio_unexport(gpio);
    lcd_close_gpio();
    closelog();
}

void finalize_daemon()
{
    int rc;
    rc = close(pid_fh);

    if (rc < 0)
	syslog(LOG_INFO, "Can not close PID file %s", PID_LOCK_FILE);
    rc = remove(PID_LOCK_FILE);

    if (rc < 0)
	syslog(LOG_INFO, "Can not remove PID file %s", PID_LOCK_FILE);

    syslog(LOG_INFO, "Dcf77 daemon stopped");
    cleanup();
}

void signal_handler(int signal) 
{
    switch(signal) {
    case SIGHUP:
	dcf77.status = syncing;
	break;
    case SIGTERM:
	finalize_daemon();
	exit(EXIT_SUCCESS);
	break;		
    }	
}

void create_daemon()
{
    int pid, sid, i; 
    char str_pid[16];

    /* Check if daemon was already created. */
    if (getppid() == 1) 
	return;

    pid = fork();	       

    if (pid < 0)	       
	exit(EXIT_FAILURE);
    if (pid > 0) {		
	exit(EXIT_SUCCESS);	/* Forked child process successfully created. */
    }
    /* Frome here lives the forked child. */
    sid = setsid();
    if (sid < 0)
	exit(EXIT_FAILURE);
    
    /* Close (std) descriptors. */
    for (i = getdtablesize(); i >= 0; --i)
	close(i);
    
    if ((chdir("/")) < 0) 
	exit(EXIT_FAILURE);

    pid_fh = open(PID_LOCK_FILE, O_RDWR | O_CREAT, 0640);
    if (pid_fh < 0) {	/* Make sure only one copy exists. */
	syslog(LOG_INFO, "Can not create PID file %s", PID_LOCK_FILE);
	exit(EXIT_FAILURE);
    }
    if (lockf(pid_fh, F_TLOCK, 0) < 0) { /* Lock the file. */
	syslog(LOG_INFO, "Can not lock PID file %s", PID_LOCK_FILE);
	exit(EXIT_FAILURE);
    }

    /* Write PID to lockfile */
    sprintf(str_pid, "%d\n", getpid());
    write(pid_fh, str_pid, strlen(str_pid));

    /* Ignore the following signals. */
    signal(SIGCHLD, SIG_IGN); 
    signal(SIGTSTP, SIG_IGN); 
    signal(SIGTTOU, SIG_IGN);
    signal(SIGTTIN, SIG_IGN);

    /* Catch these signals. */
    signal(SIGHUP, signal_handler);
    signal(SIGTERM, signal_handler);

    syslog(LOG_INFO, "Dcf77 daemon started");
}

int main(int argc, char **argv, char **envp)
{
    struct pollfd fdset[1];
    int nfds = 1;
    int timeout, rc;
    char *buf[MAX_BUF];

    unsigned char bit_array[60] = {DECODE_ERROR};
    memset(bit_array, DECODE_ERROR, 60);
    dcf77.status = syncing;
    dcf77.sec = 59;

    unsigned char flip_flank = 0;
    struct timespec timer_start, timer_stop; /* Timer value at rising flank and falling flank. */
    double time_diff_ms;	             /* Time difference in ms between rising and falling flank. */

    /* Default values. */
    arguments.daemon    = 0;	/* Do not run as daemon by default. */
    arguments.synctime  = 0;	/* Do not sync system time be default. */
    arguments.tolerance = 20;	/* 20ms seems to be a good value */
    arguments.gpio      = -1;	/* Required parameter not yet specified */

    /* Parse our arguments; every option seen by parse_opt will
       be reflected in arguments. */
    argp_parse (&argp, argc, argv, 0, 0, &arguments);

    if (arguments.daemon) {
	/* Open connection to syslog, so we know that is going on. */
	openlog(argv[0], LOG_NOWAIT | LOG_PID, LOG_USER); 
	create_daemon();
    }

    /* Activate GPIO for LCD and initialize LCD. */
    lcd_open_gpio();
    lcd_init();

    /* Export gpio and catch rising and falling flank (both). */
    gpio = arguments.gpio;
    gpio_export(gpio);
    gpio_set_dir(gpio, 0);
    gpio_set_edge(gpio, "both"); 
    gpio_fd = gpio_fd_open(gpio);

    timeout = POLL_TIMEOUT;
    
    while (1) {
	memset((void*)fdset, 0, sizeof(fdset));
	fdset[0].fd = gpio_fd;
	fdset[0].events = POLLPRI;
      
	rc = poll(fdset, nfds, timeout);      

	if (rc < 0) {
	    fprintf(stderr, "poll() failed\n");
	    syslog(LOG_ERR, "poll() failed");
	    goto cleanup;
	}
      
	if (rc == 0) {
	    fprintf(stderr, "poll() timeout\n");
	    syslog(LOG_ERR, "poll() timeout");
	    goto cleanup;
	}

	/* A rising or falling flank occured. */
	if (fdset[0].revents & POLLPRI) {
	    lseek(fdset[0].fd, 0, SEEK_SET);
	    read(fdset[0].fd, buf, MAX_BUF);

	    if (!flip_flank) {
		clock_gettime(CLOCK_REALTIME, &timer_start);
		flip_flank = 1;
	    } else {
		clock_gettime(CLOCK_REALTIME, &timer_stop);
		time_diff_ms = ((double)(timer_stop.tv_sec - timer_start.tv_sec) * 1.0e9 + (double)(timer_stop.tv_nsec - timer_start.tv_nsec)) / 1000000.0;
		flip_flank = 0;

		/* Hitting the sync minute flank. Now tell whether it is ca. 1900ms = logical 0, or ca. 1800ms = logical 1.*/
		if (abs(time_diff_ms - 1900) < arguments.tolerance || abs(time_diff_ms - 1800) < arguments.tolerance ) { 

		    dcf77.sec = 58; /* The sync flank starts at bit 58. */

		    if (!arguments.daemon)
			print_coding(bit_array, &dcf77, time_diff_ms);

		    if (abs(time_diff_ms - 1900) < arguments.tolerance)
			bit_array[dcf77.sec] = 0;
		    else
			bit_array[dcf77.sec] = 1;

		    if (dcf77.status == syncing) {
                        /* Change state to decoding. and (re)init LCD. */
			dcf77.status = decoding;
			lcd_init(); /* The LCD is sensible to noise and sometimes 
				       we display (forever) garbage. This is a good 
				       starting point to (re)initialize the LCD. */
		    }
		    else if (dcf77.status == decoding || dcf77.status == operating) { 
			/* Now we are ready to decode the dcf77 information. */

			/* Decode bit_array and update dcf77_type. */
			dcf77.status = decode_dcf77(bit_array, &dcf77);

			if (dcf77.status == operating && !arguments.daemon)
			    print_dcf77(bit_array, &dcf77);
			if (dcf77.status == operating && arguments.synctime) {
				/* (58 + 2) % 60 = 0, as flank length is approx. 1800/1900ms. */
				dcf77.sec = 0; /* 58 + 2 sec flank length */
				set_system_time(&dcf77);
			}
		    }
		    dcf77.sec = 59;
		} 
                /* Hit a 900ms flank, a logical low = 0 */
		else if (abs(time_diff_ms - 900.0) < arguments.tolerance) {
		    dcf77.sec = (dcf77.sec + 1) % 60;
		    bit_array[dcf77.sec] = 0;
		} 
                /* Hit a 800ms flank, a logical high = 1 */
		else if (abs(time_diff_ms - 800.0) < arguments.tolerance) {
		    dcf77.sec = (dcf77.sec + 1) % 60;
		    bit_array[dcf77.sec] = 1;
		} 
                /* Hit a falling flank, however we need a rising flank to keep in sync with the rising sync second. */
		else if ((abs(time_diff_ms - 100.0) < arguments.tolerance || abs(time_diff_ms - 200.0) < arguments.tolerance)) {
		    flip_flank = 1;
		    dcf77.sec = (dcf77.sec + 1) % 60;
		    bit_array[dcf77.sec] = DECODE_ERROR;
		    dcf77.status = syncing; /* Go back into syncing state. */
		}
                /* DCF77 signal is noisy and cannot be decoded. */
		else {
		    dcf77.sec = (dcf77.sec + 1) % 60;
		    bit_array[dcf77.sec] = DECODE_ERROR;
		    dcf77.status = syncing; /* Go back into syncing state. */
		}

		print_lcd(bit_array, &dcf77, time_diff_ms);
		if (!arguments.daemon)
		    print_coding(bit_array, &dcf77, time_diff_ms);
	    }
	}
	fflush(stdout);
    }

cleanup:
    if (arguments.daemon)
	syslog(LOG_INFO, "Dcf77 daemon exit");
    cleanup();
    
    exit(EXIT_SUCCESS);
}
